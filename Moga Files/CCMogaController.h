#ifndef _GAME_CONTROLLER_H_
#define _GAME_CONTROLLER_H_

#include <stddef.h>
#include "Export.h"
#include <typeinfo>
#include <ctype.h>
#include <string.h>

namespace Moga {
    
    enum KeyCodes
    {
        KEYCODE_BUTTON_A                     = 10,
		KEYCODE_BUTTON_B                     = 11,
		KEYCODE_BUTTON_X                     = 12,
		KEYCODE_BUTTON_Y                     = 13,
		KEYCODE_BUTTON_START                 = 14,
		KEYCODE_BUTTON_SELECT                = 15,
		KEYCODE_BUTTON_L1                    = 16,
		KEYCODE_BUTTON_R1                    = 17,
		KEYCODE_BUTTON_L2                    = 18,
		KEYCODE_BUTTON_R2                    = 19,
		KEYCODE_BUTTON_THUMBL                = 20,
		KEYCODE_BUTTON_THUMBR                = 21,
		KEYCODE_DPAD_UP                      = 22,
		KEYCODE_DPAD_DOWN                    = 23,
		KEYCODE_DPAD_LEFT                    = 24,
		KEYCODE_DPAD_RIGHT                   = 25
    };
    
    enum Axis
    {
        AXIS_X                              = 0,
        AXIS_Y                              = 1,
        AXIS_Z                              = 11,
        AXIS_RZ                             = 14,
        AXIS_LTRIGGER                       = 17,
        AXIS_RTRIGGER                       = 18
    };
    
    enum Actions
    {
        ACTION_CONNECTED                    = 0,
        ACTION_DISCONNECTED                 = 1,
        ACTION_CONNECTING                   = 2
    };
    
    enum States
    {
        STATE_CONNECTION                     = 10,
        STATE_POWER_LOW                      = 11,
        STATE_SUPPORTED_PRODUCT_VERSION      = 12,
        STATE_CURRENT_PRODUCT_VERSION        = 13
    };

class TypeInfo
{
public:
    virtual long getClassTypeInfo() = 0;
};
    
    static inline unsigned int getHashCodeByString(const char *key)
    {
        unsigned int len = strlen(key);
        const char *end=key+len;
        unsigned int hash;
        
        for (hash = 0; key < end; key++)
        {
            hash *= 16777619;
            hash ^= (unsigned int) (unsigned char) toupper(*key);
        }
        return (hash);
    }
    
/**
@class          MOGA
@brief          offer a VERY simple interface to use the iOS Moga Controller
*/

class EXPORT_DLL CCMogaController : public TypeInfo
{
public:
    CCMogaController();
    ~CCMogaController();

    virtual long getClassTypeInfo() {
        return getHashCodeByString(typeid(Moga::CCMogaController).name());
    }

    /**
    @brief Get the shared Engine object,it will new one when first time be called
    */
    static CCMogaController* shared();

    /**
    @brief Release the shared Engine object
    @warning It must be called before the application exit, or a memroy leak will be casued.
    */
    static void end();

    /**
     @brief get the connection state of the controller
     */
    unsigned int getControllerState(int state);
    
    /**
     @brief get the assigned player id
     */
    unsigned int getPlayerIndex();
    
    /**
    @brief get the number of connected controllers
    */
    unsigned int getControllerCount();
    
    /**
     @brief returns a button state
     @param the button you wish to return
     */
    float getButtonValue(int keyCode);
    
    /**
     @brief returns an axis value
     @param the axis you wish to return
     */
    float getAxisValue(int axis);

};

} // end of namespace Moga

#endif // _GAME_CONTROLLER_H_

