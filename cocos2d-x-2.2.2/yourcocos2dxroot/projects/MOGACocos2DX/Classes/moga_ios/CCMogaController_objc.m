#import "CCMogaController_objc.h"
#import <GameController/GameController.h>

@implementation CCMogaController

static CCMogaController* instance = nil;
static GCController * controller = nil;

const int   KEYCODE_BUTTON_A                     = 10,
            KEYCODE_BUTTON_B                     = 11,
            KEYCODE_BUTTON_X                     = 12,
            KEYCODE_BUTTON_Y                     = 13,
            KEYCODE_BUTTON_L1                    = 16,
            KEYCODE_BUTTON_R1                    = 17,
            KEYCODE_BUTTON_L2                    = 18,
            KEYCODE_BUTTON_R2                    = 19,
            KEYCODE_BUTTON_THUMBL                = 20,
            KEYCODE_BUTTON_THUMBR                = 21,
            KEYCODE_DPAD_UP                      = 22,
            KEYCODE_DPAD_DOWN                    = 23,
            KEYCODE_DPAD_LEFT                    = 24,
            KEYCODE_DPAD_RIGHT                   = 25;

const int   ACTION_CONNECTED                    = 0,
            ACTION_DISCONNECTED                 = 1,
            ACTION_CONNECTING                   = 2;

const int   STATE_CONNECTION                    = 10,
            STATE_POWER_LOW                     = 11,
            STATE_SUPPORTED_PRODUCT_VERSION     = 12,
            STATE_CURRENT_PRODUCT_VERSION       = 13;

const int   AXIS_X                              = 0,
            AXIS_Y                              = 1,
            AXIS_Z                              = 11,
            AXIS_RZ                             = 14,
            AXIS_LTRIGGER                       = 17,
            AXIS_RTRIGGER                       = 18;

+ (CCMogaController*)shared
{
    @synchronized(self)
    {
        if (instance == nil)
        {
            instance = [[self alloc] init];
            [instance registerConnectionNotification];
        }
    }
    return instance;
}

+ (id) alloc
{
    @synchronized(self)
    {
        NSAssert(instance == nil, @"Attempted to allocate a second instance of a singleton.");
        return [super alloc];
    }
    return nil;
}

-(id) init
{
    if((self=[super init]))
    {
        isPaused_ = NO;
        connectionStatus_ = ACTION_CONNECTED;
    }
    return self;
}

// Memory
- (void) dealloc
{
    controller = nil;
    [super dealloc];
}

+(void) end
{
    controller = nil;
    [GCController release];
    instance = nil;
}

- (int)getControllerState:(int) state
{
    switch (state)
    {
        case STATE_CONNECTION:
             return connectionStatus_;
            break;
    }
    return -1;
}

- (int)getPlayerIndex
{
    if (connectionStatus_ == ACTION_CONNECTED)
    {
        return controller.playerIndex;
    }
    return -1;
}

- (int)getControllerCount
{
    return [[GCController controllers] count];
}

#pragma mark Controls

-(float) getButtonValue:(int) keyCode
{
    switch (keyCode)
    {
        case KEYCODE_BUTTON_A:
            return controller.gamepad.buttonA.value;
            break;
        case KEYCODE_BUTTON_B:
            return controller.gamepad.buttonB.value;
            break;
        case KEYCODE_BUTTON_X:
            return controller.gamepad.buttonX.value;
            break;
        case KEYCODE_BUTTON_Y:
            return controller.gamepad.buttonY.value;
            break;
        case KEYCODE_BUTTON_L1:
            return controller.gamepad.leftShoulder.value;
            break;
        case KEYCODE_BUTTON_R1:
            return controller.gamepad.rightShoulder.value;
            break;
        case KEYCODE_DPAD_UP:
            return controller.gamepad.dpad.up.value;
            break;
        case KEYCODE_DPAD_DOWN:
            return controller.gamepad.dpad.down.value;
            break;
        case KEYCODE_DPAD_LEFT:
            return controller.gamepad.dpad.left.value;
            break;
        case KEYCODE_DPAD_RIGHT:
            return controller.gamepad.dpad.right.value;
            break;
        default:
            return 0;
            break;
    }
}

-(float) getAxisValue:(int) axis
{
    switch (axis)
    {
        case AXIS_X:
            return controller.extendedGamepad.leftThumbstick.xAxis.value;
            break;
        case AXIS_Y:
            return controller.extendedGamepad.leftThumbstick.yAxis.value;
            break;
        case AXIS_Z:
            return controller.extendedGamepad.rightThumbstick.xAxis.value;
            break;
        case AXIS_RZ:
            return controller.extendedGamepad.rightThumbstick.yAxis.value;
            break;
        case AXIS_LTRIGGER:
            return controller.extendedGamepad.leftTrigger.value;
            break;
        case AXIS_RTRIGGER:
            return controller.extendedGamepad.rightTrigger.value;
            break;
        default:
            return 0;
            break;
    }
}

#pragma mark SetupNotifications

- (void)registerConnectionNotification
{
    connectionStatus_ = ACTION_CONNECTING;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectController:)
                                                 name:UIApplicationDidFinishLaunchingNotification object:nil];
}

- (void)connectController:(NSNotification *)notification
{
    // Receive notifications when a controller connects or disconnects.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameControllerDidConnect:) name:GCControllerDidConnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameControllerDidDisconnect:) name:GCControllerDidDisconnectNotification object:nil];
}

#pragma mark Notifications

- (void)gameControllerDidConnect:(NSNotification *)notification
{
    controller = notification.object;
    
    controller.playerIndex = 0;
    
    connectionStatus_ = ACTION_CONNECTED;
    
    controller.controllerPausedHandler = ^(GCController* controller)
    {
        if (isPaused_)
        {
            isPaused_ = false;
        }
        else
        {
            isPaused_ = true;
        }
    };
}

- (void)gameControllerDidDisconnect:(NSNotification *)notification
{
    controller = notification.object;
    connectionStatus_ = ACTION_DISCONNECTED;
}

@end