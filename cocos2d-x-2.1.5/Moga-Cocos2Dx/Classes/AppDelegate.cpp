//
//  Moga_Cocos2DxAppDelegate.cpp
//  Moga-Cocos2Dx
//
//  Created by Mark Dunn on 16/09/2013.
//  Copyright BDA 2013. All rights reserved.
//

#include "AppDelegate.h"

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "MainScene.h"

#define DESIGN_RESOLUTION_IPHONE        0
#define DESIGN_RESOLUTION_IPHONE4       1
#define DESIGN_RESOLUTION_IPHONE5       2
#define DESIGN_RESOLUTION_IPAD          3
#define DESIGN_RESOLUTION_IPADRETINA    4

int iOSModel;

USING_NS_CC;
using namespace CocosDenshion;

typedef struct tagResource
{
    cocos2d::CCSize size;
    char directory[100];
}Resource;

static Resource largeResource  =  { cocos2d::CCSizeMake(1024, 768),  "Assets"   };

AppDelegate::AppDelegate()
{

}

AppDelegate::~AppDelegate()
{
}

bool AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());

    CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();
    
    // Get the Device Screen Size
    CCSize frameSize = pEGLView->getFrameSize();
    CCSize designResolutionSize;
    
    // Set Device Resolution
    if (frameSize.width > 1136)
    {
        CCLog("iPad Retina Discovered");
        iOSModel = DESIGN_RESOLUTION_IPADRETINA;
        designResolutionSize = cocos2d::CCSizeMake(2048, 1536);
    }
    else if (frameSize.width > 1024)
    {
        CCLog("iPhone 5 Discovered");
        iOSModel = DESIGN_RESOLUTION_IPHONE5;
        designResolutionSize = cocos2d::CCSizeMake(1136, 640);
    }
    else if (frameSize.width > 960)
    {
        CCLog("iPad Discovered");
        iOSModel = DESIGN_RESOLUTION_IPAD;
        designResolutionSize = cocos2d::CCSizeMake(1024, 768);
    }
    else if (frameSize.width > 480)
    {
        CCLog("iPhone 4 Discovered");
        iOSModel = DESIGN_RESOLUTION_IPHONE4;
        designResolutionSize = cocos2d::CCSizeMake(960, 640);
    }
    else
    {
        CCLog("iPhone Discovered");
        iOSModel = DESIGN_RESOLUTION_IPHONE;
        designResolutionSize = cocos2d::CCSizeMake(480, 320);
    }
    
    // Set the design resolution
    pEGLView->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, kResolutionShowAll);
    
    std::vector<std::string> searchPath;
    
    searchPath.push_back(largeResource.directory);
    pDirector->setContentScaleFactor(largeResource.size.height/designResolutionSize.height);
    
    // turn on display FPS
    pDirector->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    CCScene *pScene = MainScene::scene();

    // run
    pDirector->runWithScene(pScene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    CCDirector::sharedDirector()->stopAnimation();
    SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
    SimpleAudioEngine::sharedEngine()->pauseAllEffects();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    CCDirector::sharedDirector()->startAnimation();
    SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    SimpleAudioEngine::sharedEngine()->resumeAllEffects();
}
